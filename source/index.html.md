---
title: Bareo API Reference

language_tabs:
  - curl
  - python

toc_footers:
  - <a href='https://app.bareo.io/'>Sign Up for a Bareo Account</a>
  - <a href='https://github.com/tripit/slate'>Documentation Powered by Slate</a>

includes:
  - errors

search: true
---

# Introduction

Welcome to the Bareo API! You can use our API to access Bareo API
endpoints, which can get, set, update, and sometimes delete
information on Items, Recipes, Stock, Sales, Auctions, Users, and
Accounts.

We have language bindings in curl, python, and objective-c! You can
view code examples in the dark area to the right, and you can switch
the programming language of the examples with the tabs in the top
right.


This example API documentation page was created with
[Slate](https://github.com/tripit/slate). Feel free to edit it and use
it as a base for your own API's documentation.

# Authentication

> Make sure to replace `YOUR_AUTH_TOKEN_HERE` with the auth_token
> returned from the authentication API call.

The Bareo API uses HTTP headers to allow access to the API. The
header must have a value of the user's auth_token key that is returned
from the the authentication call.

The Bareo API expects the auth_token to be included in all API
requests to the server in a header that looks like the following:

`X-Authorization: YOUR_AUTH_TOKEN_HERE`

<aside class="notice">
You must replace <code>YOUR_AUTH_TOKEN_HERE</code> with the value of
the "auth_token" key in the response of a successful call to the
authenticate method.
</aside>

## Log a User in


> To authorize a user (log them in), use this code:

```python

from bareo import *

api = BareoAPI()
user = BareoUser({'username':'test,'password':'zwick'})
api.user = user

response = api.user.authenticate()
print(api.user)
```

```shell
# With shell, you can just pass the correct header with each request
curl 'https://api.bareo.io/v2/auth'
  --data '{"username":"test","password":"zwick"}'
```

> The above API calls with return the following JSON

```json
{
    "username": "test",
    "auth_token": "e0a51ef2bb0895cc579b092f90f17090",
    "email": "zwick@bareo.io",
    "phone": "4193536604",
    "privilege": 2,
    "id": 2
}
```

### HTTP Request

`POST https://api.bareo.io/v2/auth`

## Log a User out

```python
from bareo import *

api = BareoAPI()
api.user.username = "test"
api.user.password = "test"

response = api.authenticate()
print(api.user)

response = api.deauthenticate()
print(api.user)
```

```shell
curl 'https://api.bareo.io/v2/auth/2'
  -H 'X-Authorization: YOUR_AUTH_TOKEN_HERE'
  -X DELETE
```

> The above API calls return an HTTP 204 No Content

### HTTP Request

`DELETE https://api.bareo.io/v2/auth/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the User object to de-authenticate

# Users

Within Bareo, a `User` is a username/password pair that can be used to
log in with. A `User` belongs to at least one `Account`, but can
easily belong to multiple `Account` objects.

## Create a new Bareo User

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/users'
  --data '{"username":"jenny","password":"jenny","email":"jenny@bareo.io","phone":"8675309","privilege":"1"}'
```

> The above API call will return JSON structured as follows:

```json
{
    "username": "jenny",
    "id": 37,
    "phone": "8657309",
    "accounts": [{
        "account_name": "",
        "role": 1,
        "account_type": 0,
        "id": 32,
        "allowed": 1
    }],
    "email_confirmed": false,
    "privilege": "1",
    "email": "jenny@bareo.io"
}
```

### HTTP Request

`POST https://api.bareo.io/v2/users`

## Create a new Supplier User

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/users'
  --data '{"username":"jenny","password":"jenny",email:"jenny@bareo.io","phone":"8675309","privilege":"1","supplier":"1"}'
```

> This API call returns a JSON respone formatted as follows:

```json
{
    "username": "jenny",
    "id": 38,
    "phone": "8657309",
    "accounts": [{
        "account_name": "",
        "role": 5,
        "account_type": 3,
        "id": 33,
        "allowed": 1
    }],
    "email_confirmed": false,
    "privilege": "1",
    "email": "jenny@bareo.io"
}
```

### HTTP Request

`POST https://api.bareo.io/v2/users`

## Get a single User

```python
TBD
```

```shell
curl 'https://api.bareo.io/v3/users/2'
  -H 'X-Authorization: YOUR_AUTH_TOKEN_HERE'
```

> This API call returns a JSON response formatted as follows:

```json
{
    "username": "test",
    "email": "zwick@bareo.io",
    "phone": "4193536604",
    "accounts": [{
        "account_name": "",
        "role": 1,
        "account_type": 0,
        "id": 1,
        "allowed": 1
    }],
    "privilege": 2,
    "id": "2"
}
```

### HTTP Request

`GET https://api.bareo.io/v2/users/<ID>`

### URL Paramaters

Parameter | Description
--------- | -----------
ID | The ID of the User of which to retrieve a full representation

# Accounts

Within Bareo, an `Account` is the top object in the data heirarchy in
that every other type of object belongs to at least one `Account`, and
an `Account` belongs to nothing except itself. You can think of an
`Account` object as a single pantry of `Item`, `Recipe`, `Stock`, and
`Sale` objects.

## Get All Accounts for the User

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/accounts'
  -H 'X-Authorization: YOUR_AUTH_TOKEN_HERE'
```

> The above command returns JSON structured like this:

```json
{
    "accounts": [{
        "account_type": 0,
        "users": [{
            "role": 1,
            "id": 2
        }],
        "payment_token": null,
        "image_id": null,
        "allowed": 1,
        "id": 1,
        "account_name": ""
	},{
		"account_type": 1,
        "users": [{
            "role": 1,
            "id": 2
        }],
        "payment_token": null,
        "image_id": null,
        "allowed": 10,
        "id": 2,
        "account_name": "Bob's Burgers"
    }]
}
```

### HTTP Request

`GET https://api.bareo.io/v2/accounts`

## Get a Specific Account

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/accounts/1'
  -H 'Xy-Authorization: YOUR_AUTH_TOKEN_HERE'
```

> The above command returns JSON structured like this:

```json
{
    "accounts": [{
        "account_type": 0,
        "users": [{
            "role": 1,
            "id": 2
        }],
        "payment_token": null,
        "image_id": null,
        "allowed": 1,
        "id": 1,
        "account_name": ""
	}]
}
```

### HTTP Request

`GET https://api.bareo.io/v2/accounts/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the `Account` object to retrieve

## Create a new Account

```python
TDB
```

```shell
curl http:TRSDRSTAST
```

# Items

## Get All Items for the User

```python

from bareo import *

api = BareoAPI()
api.user.username = "test"
api.user.password = "test"

response = api.authenticate()

if api.user["auth_token"] is None:
    exit()

api.fetch_items()
print(api.items)
```

```shell
curl 'https://api.bareo.io/v2/items'
  -H 'X-Authorization: YOUR_AUTH_TOKEN_HERE'
```

> The above command returns JSON structured like this:

```json
{
    "items": [{
        "units": "cups",
        "expirations": [{
            "units": "cups",
            "shelf_life": "2016-12-31 00:00:00",
            "location": "pantry",
            "quantity": 14.999984973893694
        }],
        "name": "flour",
        "price": 0.25,
        "description": "all purpose",
        "upc": null,
        "account_id": 1,
        "image_id": 0,
        "threshold": 0.0,
        "id": 15,
        "quantity": 7.000009894828372
    }, {
        "units": "mg",
        "expirations": [],
        "name": "Sprite. 2 Liter Bottle",
        "price": 5.74,
        "description": "Sprite Sprite",
        "upc": "049000050158",
        "account_id": 1,
        "image_id": null,
        "threshold": 0.0,
        "id": 16,
        "quantity": 0.0
    }]
}
```

This endpoint retrieves all `Item` objects for the user with the
`auth_token` passed in via the `X-Authorization` header.

### HTTP Request

`GET https://api.bareo.io/v2/items`

## Get All Items for an Account

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/accounts/1/items'
  -H 'X-Authorization: YOUR_AUTH_TOKEN_HERE'
```

> The above command returns JSON structured like this:

```json
{
    "items": [{
        "units": "cups",
        "expirations": [{
            "units": "cups",
            "shelf_life": "2016-12-31 00:00:00",
            "location": "pantry",
            "quantity": 14.999984973893694
        }],
        "name": "flour",
        "price": 0.25,
        "description": "all purpose",
        "upc": null,
        "account_id": 1,
        "image_id": 0,
        "threshold": 0.0,
        "id": 15,
        "quantity": 7.000009894828372
    }, {
        "units": "mg",
        "expirations": [],
        "name": "Sprite. 2 Liter Bottle",
        "price": 5.74,
        "description": "Sprite Sprite",
        "upc": "049000050158",
        "account_id": 1,
        "image_id": null,
        "threshold": 0.0,
        "id": 16,
        "quantity": 0.0
    }]
}
```

This endpoint retrieves all of the `Item` objects in the given
`Account`. The `User` that corresponds to the `auth_token` being
passed in via the `X-Authorization` header must belong to the
`Account` being requested. The API will check this, and return either
data or an HTTP error code.

### HTTP Request

`GET https://api.bareo.io/v2/accounts/<ID>/items`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the Account of which to retrieve all Item objects

## Get a Specific Item

```python
from bareo import *

api = BareoAPI()
api.user.username = "test"
api.user.password = "test"

response = api.authenticate()

api.fetch_item(15)
print(next(item for item in api.items if int(item['id']) == 15))
```

```shell
curl 'https://api.bareo.io/v2/items/15'
  -H 'X-Authorization: YOUR_AUTH_TOKEN_HERE'
```

> The above command returns JSON structured like this:

```json
{
    "expirations": [{
        "units": "cups",
        "shelf_life": "2016-12-31 00:00:00",
        "location": "pantry",
        "quantity": 14.999984973893694
    }],
    "price": 0.25,
    "image_id": 0,
    "threshold": 0.0,
    "id": "15",
    "account_id": 1,
    "name": "flour",
    "description": "all purpose",
    "upc": null,
    "units": "cups",
    "quantity": 7.000009894828372
}
```

This endpoint retrieves a specific `Item` object with the given `id`.


### HTTP Request

`GET https://api.bareo.io/v2/items/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The `id` of the `Item` object to retrieve

## Create a new Item

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/items/' -H 'X-Authorization:
e2f1b2206bc8baeda8ff905345895cff' --data
'{"name":"flour","description":"all purpose",
"price":"0.25","image_id":"","account_id":"1","units":"cups","threshold":"5","expirations":[{"quantity":"15","units":"cups","location":"pantry","shelf_life":"2016-12-31 00:00:00"}]}'
```

> The above command returns JSON structured like this:

```json
{
    "expirations": [{
        "units": "cups",
        "shelf_life": "2016-12-31 00:00:00",
        "location": "pantry",
        "quantity": 14.999984973893694
    }],
    "price": 0.25,
    "image_id": 0,
    "threshold": 0.0,
    "id": "15",
    "account_id": 1,
    "name": "flour",
    "description": "all purpose",
    "upc": null,
    "units": "cups",
    "quantity": 7.000009894828372
}
```

This endpoint creates a new Item object with the provided attributes.


### HTTP Request

`POST https://api.bareo.io/v2/items/`

## Create a new Item by UPC

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/items/' -H 'X-Authorization:
e2f1b2206bc8baeda8ff905345895cff' --data
'{"upc":"049000050158","image_id":"","account_id":"1","threshold":"5","expirations":[{"location":"pantry","shelf_life":"2016-12-31 00:00:00"}]}'
```

> Creating a new Item via this endpoint returns the full JSON
> representation of the Item


```json
{
    "expirations": [{
        "units": "cups",
        "shelf_life": "2016-12-31 00:00:00",
        "location": "pantry",
        "quantity": 14.999984973893694
    }],
    "price": 0.25,
    "image_id": 0,
    "threshold": 0.0,
    "id": "15",
    "account_id": 1,
    "name": "flour",
    "description": "all purpose",
    "upc": null,
    "units": "cups",
    "quantity": 7.000009894828372
}
```

### HTTP Request

`POST https://api.bareo.io/v2/items`

## Update a specific Item

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/items/2' -H 'X-Authorization:
e2f1b2206bc8baeda8ff905345895cff' --data
'{"name":"apples","description":"Granny
Smith","price":"1.0","image_id":"","account_id":"2","quantity":"30","shelf_life":"2014-12-15
00:00:00","units":"pieces","location":"fruit shelf","threshold":"10"}'
-X PUT
```

> This request returns a JSON structure of the newly updated Item
> object

```json
{
    "expirations": [{
        "units": "cups",
        "shelf_life": "2016-12-31 00:00:00",
        "location": "pantry",
        "quantity": 14.999984973893694
    }],
    "price": 0.25,
    "image_id": 0,
    "threshold": 0.0,
    "id": "15",
    "account_id": 1,
    "name": "flour",
    "description": "all purpose",
    "upc": null,
    "units": "cups",
    "quantity": 7.000009894828372
}
```

A PUT request to this endpoint allows modifying an existing Item
object.

### HTTP Request

`PUT https://api.bareo.io/v2/items/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the Item which is to be updated

## Delete a Specific Item

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/items/2' -H 'X-Authorization:
e2f1b2206bc8baeda8ff905345895cff' -X DELETE
```

> A successful deletion request returns a 204 - No Content response

Delete a specific Item object. This action cannot be undone.

### HTTP Request

`DELETE https://api.bareo.io/v2/items/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The ID of the Item object to delete

# Recipes

## Get all Recipes for the User

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/recipes'
  -H 'X-Authorization: YOUR_AUTH_TOKEN_HERE'
```

> The above command returns JSON structured like this:

```json

```

This endpoint retrieves all `Recipe` objects for the user with the
`auth_token` passed in via the `X-Authorization` header.

### HTTP Request

`GET https://api.bareo.io/v2/recipes`

## Get All Recipes for an Account

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/accounts/1/recipes'
  -H 'X-Authorization: YOUR_AUTH_TOKEN_HERE'
```

> The above command returns JSON structured like this:

```json

```

This endpoint retrieves all of the `Recipe` objects in the given
`Account`. The `User` that corresponds to the `auth_token` being
passed in via the `X-Authorization` header must belong to the
`Account` being requested. The Bareo API will check this, and return
either data or an HTTP error.

### HTTP Request

`GET https://api.bareo.io/v2/accounts/<ID>/recipes`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The `id` of the `Account` of which to retrieve all `Recipe`
objects

## Get a Specific Recipe

```python
TBD
```

```shell
curl 'https://api.bareo.io/v2/recipes/3'
-H 'X-Authorization: YOUR_AUTH_TOKEN_HERE'
```

> The above command returns JSON structured like this:

```json

```

This endpoint retrieves a specific `Recipe` object with the given
`id`.

### HTTP Request

`GET https://api.bareo.io/v2/recipes/<ID>`

### URL Parameters

Parameter | Description
--------- | -----------
ID | The `id` of the `Recipe` object to retrieve
