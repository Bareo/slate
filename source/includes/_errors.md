# HTTP Codes

The Bareo API uses the following HTTP codes:


Status Code | Status Text | Meaning
----------- | ----------- | -------
200 | OK | Your request succeeded and returned the JSON of the object that your request interacted with.
204 | No Content | Your request was successful, but there is nothing to return. This generally means that you deleted an object.
400 | Bad Request | Your request is malformed
401 | Unauthorized | Your request either does not contain a valid X-Authorization header, or the value of the X-Authorization header is not valid
403 | Forbidden | Your request has a valid X-Authorization header, but you are requesting an object that your auth_token does not have access to
404 | Not Found | Your request URL cannot be found
405 | Method Not Allowed | Your request attempts a forbidden method
500 | Internal Server Error | The Bareo API had something go wrong internally. Try your request again later
503 | Service Unavailable | The Bareo API is temporarially offline for maintanance. Try your request again later.
